\documentclass[a4paper,12pt,reqno,oneside]{amsart}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[dvipsnames]{xcolor}
\usepackage{typearea}
\usepackage{hyperref}
\usepackage[capitalise,nameinlink]{cleveref}
\usepackage{mathtools}
\usepackage{indentfirst}
\usepackage{enumitem}

\crefformat{enumi}{#2(#1)#3}

\definecolor{color0}{RGB}{34,113,178}
\definecolor{color1}{RGB}{61,183,233}
\definecolor{color2}{RGB}{247,72,165}
\definecolor{color3}{RGB}{53,155,115}
\definecolor{color4}{RGB}{213,94,0}
\definecolor{color5}{RGB}{230,159,0}
\definecolor{color6}{RGB}{114,228,97}
\definecolor{color7}{RGB}{53,37,94}
\definecolor{color8}{RGB}{128,0,103}

\colorlet{blue}{color0}
\colorlet{pink}{color2}

\hypersetup{
  pagebackref,
  colorlinks,
  linkcolor=blue,
  citecolor=pink,
  urlcolor=pink
}

\theoremstyle{plain}
\newtheorem{prop}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\theoremstyle{definition}
\newtheorem*{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem*{remark}{Remark}

\setlength{\parindent}{0cm}
\setlength{\parskip}{0.5em}

\setlist{listparindent=\parindent,parsep=\parskip}

\newcommand{\crefp}[2]{\hyperref[#2]{\namecref{#1}~\labelcref*{#1}~(\ref*{#2})}}

\newcommand{\from}{\colon}
\newcommand{\xto}[1]{\xrightarrow{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}

\newcommand{\id}{\mathrm{id}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Conf}{\mathrm{Conf}}
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)}
\DeclareEmphSequence{\bfseries,\itshape,\upshape}

\DeclareMathOperator{\pr}{pr}
\DeclareMathOperator{\sk}{sk}
\DeclareMathOperator{\st}{st}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Int}{Int}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand{\maxl}[1]{\max\limits_{#1}}
\newcommand{\minl}[1]{\min\limits_{#1}}
\newcommand{\argmaxl}[1]{\argmax\limits_{#1}}
\newcommand{\argminl}[1]{\argmin\limits_{#1}}

\author{Alexey Gorelov}
\title{Some propositions on double paths}

\begin{document}
\maketitle

We denote by $I$ the unit segment $[0, 1]$. Additionally, the vertices of the unit square $I^2$ are
denoted by $A = (0, 0)$, $B = (1, 0)$, $C = (1, 1)$, and $D = (0, 1)$.

The expressions $\min_C f(x)$ and $\max_C f(x)$ denote $\min_{x \in C} f(x)$ and $\max_{x \in C} f(x)$,
respectively. Furthermore, $\argmax_C f(x)$ refers to \emph{any} $x \in C$ such that $f(x) = \max_C f(x)$.

\begin{lemma}[see Lemma 1 in~\cite{mountain}]\label{lemma:sep}
  Let $P$ be a compact subpolyhedron of $I^2$, and let $p, q \in P$ be a pair of points. Then either
  \begin{enumerate}
  \item $p$ and $q$ lie in the same connected component of $P$;
  \item there exists a curve $\gamma \subset I^2 \setminus P$ that divides $I^2$ into two
    connected components, with the points $p$ and $q$ lying in different ones.
  \end{enumerate}
\end{lemma}
\begin{proof}
  Take a triangulation of $I^2$ that triangulates $P$ and $\{p, q\}$ as subcomplexes. Take its
  second derived triangulation, and let $N_s(C)$ be the simplicial neighborhood of the connected
  component $C$ of $P$ containing $p$.

  Thus, $\partial N_s(C)$ is a finite collection $\{ \tau_i \}_i$ of curves. Moreover, it is easy to check that the
  vertices of $\partial N_s(C)$ lying in the interior of the square have degree two; therefore, each
  curve $\tau_i$ is either closed or have its endpoints lying in $\partial I^2$.

  Each curve $\tau_i$ divides $I^2$ into two connected components $T_0^i$ and
  $T_1^i$.\footnote{Theorem 1 in~\cite{moise}.} For each $x \in P$, let $v(x)$ be a vector where
  $v_i(x) = 1$ if $x \in T_1^i$ and $v_i(x) = 0$ if $x \in T_0^i$. It is clear that $v(x) = v(y)$ if
  and only if $x$ and $y$ lie in the same connected component of $I^2 \setminus \bigcup_i
  \tau_i$.\footnote{This can be easily proven by induction.} Noting that any path between $p$ and
  $q$ intersects $\bigcup_i \tau_i$,\footnote{We can assume that the path is in the 1-skeleton of
    the triangulation.} we conclude that $p$ and $q$ lie in different connected components, and
  hence $v(p) \neq v(q)$.\footnote{We use that connectedness implies path-connectedness for open
    subspaces of $\R^2$.}

  Choose $k$ such that $v_k(p) \neq v_k(q)$. As we have previously mentioned, this implies that $p$ and $q$ lie in
  different connected components of $I^2 \setminus \tau_k$. Therefore, by setting $\gamma = \tau_k$, we
  conclude the proof.
\end{proof}

\begin{prop}\label{prop:curves}
  Let $f, g \from I \to \R$ be piecewise linear maps. Let $P$ be a subpolyhedron of $I^2$ defined as
  $P = \{ (x, y) \mid f(x) = g(y) \}$.

  The following statements regarding the existence of a curve $\gamma \in I^2 \setminus P$ hold:
  \begin{enumerate}
  \item There is no $\gamma$ connecting sides $AB$ and $CD$ if and only if $\max_I f(x) \leq \max_I g(y)$
    and $\min_I f(x) \geq \min_I g(y)$
  \item There is no $\gamma$ connecting sides $BC$ and $DA$ if and only if $\max_I f(x) \geq \max_I g(y)$
    and $\min_I f(x) \leq \min_I g(y)$;
  \item There is no $\gamma$ connecting
    \begin{enumerate}[noitemsep]
    \item $AB$ and $DA$
    \item $AB$ and $BC$
    \item $BC$ and $CD$
    \item $CD$ and $DA$
    \end{enumerate}
    if and only if for all $s, t \in I$ we have either
    \[
      \hspace*{\dimexpr+\leftmargini+\leftmarginii}
      \max_{I_s} f(x) \geq \max_{I_t} g(y) \geq \min_{I_t} g(y) \geq \min_{I_s} f(x)
    \]
    or
    \[
      \hspace*{\dimexpr+\leftmargini+\leftmarginii}
      \max_{I_t} g(y) \geq \max_{I_s} f(x) \geq \min_{I_s} f(x) \geq \min_{I_t} g(y)
    \]

    Here, $I_s = [0, s]$ for cases (a) and (d), and $I_s = [s, 1]$ for cases (b) and (c). Similarly,
    $I_t = [0, t]$ for cases (a) and (b), and $I_t = [t, 1]$ otherwise.
  \end{enumerate}
\end{prop}

\begin{proof}
  First, let $F \from I^2 \to \R$ be a function defined as $F(x, y) = f(x) - g(y)$. Therefore, $P =
  F^{-1}(0)$. We write $F(\gamma) > 0$ or $F(\gamma) < 0$ if the corresponding inequality holds for
  the value on each point $t \in \gamma$. Clearly, $\gamma \in I^2 \setminus P$ if and only if
  either $F(\gamma) > 0$ or $F(\gamma) < 0$.

  \begin{enumerate}
  \item Assume $\max_I f(x) > \max_I g(y)$. Choose
    $\gamma = \{\argmax_I f(x) \} \times I$. Clearly, $F(\gamma) > 0$.  When
    $\min_I f(x) < \min_I g(y)$, take $\gamma = \{ \argmin_I f(x) \} \times I$. In this case
    $F(\gamma) < 0$.

    Now, assume $\gamma$ exists. When $F(\gamma) > 0$, we have
    \[
      \hspace*{\dimexpr+\leftmargini}
      \exists x'\, (x', \argmax_{I} g(y)) \in \gamma \ \Rightarrow \
      \exists x'\, f(x') > \max_{I} g(y) \ \Rightarrow \
      \max_{I} f(x) > \max_{I} g(y)
    \]
    And when $F(\gamma) < 0$, we have
    \[
      \hspace*{\dimexpr+\leftmargini}
      \exists x'\, (x', \argmin_{I} g(y)) \in \gamma \ \Rightarrow \
      \exists x'\, f(x') < \min_{I} g(y) \ \Rightarrow \
      \min_{I} f(x) < \min_{I} g(y)
    \]
  \item A proof of the second statement is essentially the same as the proof of the first one, with $g(y)$ and $f(x)$ swapped.
  \item

    Assume the conditions of the third statement fail. It implies that there exist $s, t \in I$ such that either
    \[
      \hspace*{\dimexpr+\leftmargini}
      \begin{array}{lcr}
        &\begin{dcases}
          \max_{I_s} f(x) > \max_{I_t} g(y) \\
          \min_{I_s} f(x) > \min_{I_t} g(y) \\
        \end{dcases}
        &\mathrm{or}\hspace{1em}
          \begin{dcases}
            \max_{I_s} f(x) < \max_{I_t} g(y) \\
            \min_{I_s} f(x) < \min_{I_t} g(y) \\
          \end{dcases}
      \end{array}
    \]

    In the first case, consider the union of lines $x = \argmax_{I_s} f(x)$ and
    $y = \argmin_{I_t} g(y)$ within a square $I_s \times I_t$:
    \[
      \hspace*{\dimexpr+\leftmargini}
      X = \left(\left\{ (x, y) \mid x = \argmax_{I_s} f(x)\right\} \cup \left\{ (x, y) \mid y = \argmin_{I_t} g(y) \right\}\right) \cap (I_s \times I_t)
    \]
    Since $\max_{I_s} f(x) > g(y)$ and $\min_{I_t} g(y) < f(x)$ in $I_s \times I_t$, we have
    $F(X) > 0$. However, $X$ is a cross lying in $I_s \times I_t$, and it is easy to see that it
    contains a curve connecting two sides of $I^2$ with a non-empty intersection with
    $I_s \times I_t$. It can be observed that these sides are exactly the sides of $I_s \times I_t$
    that are mentioned in the corresponding item of the proposition.

    In the second case, we choose $X$ as the union of lines $x = \argmin_{I_s} f(x)$ and
    $y = \argmax_{I_t} g(y)$ within $I_s \times I_t$, and observe that $F(X) < 0$, the rest of the
    reasoning remains the same.

    Now, assume the conditions are satisfied but $\gamma$ exists. Let $\pr_1(\gamma) = I_s$ and
    $\pr_2(\gamma) = I_t$.

    If $F(\gamma) > 0$, we have
    \[
      \hspace*{\dimexpr+\leftmargini}
      \begin{aligned}
        &\exists y'\, (\argmin_{I_s} f(x), y') \in \gamma &\Rightarrow&
        &\exists y'\, &\min_{I_s} f(x) > g(y') &\Rightarrow&
        &\min_{I_s} f(x) > \min_{I_t} g(y) \\
        &\exists x'\, (x', \argmax_{I_t} g(y)) \in \gamma &\Rightarrow&
        &\exists x'\, &f(x') > \max_{I_t} g(y) &\Rightarrow&
        &\max_{I_s} f(x) > \max_{I_t} g(y) \\
      \end{aligned}
    \]

    Similarly, if $F(\gamma) < 0$, we have
    \[
      \hspace*{\dimexpr+\leftmargini}
      \begin{aligned}
        &\exists y'\, (\argmax_{I_s} f(x), y') \in \gamma &\Rightarrow&
        &\exists y'\, &\max_{I_s} f(x) < g(y') &\Rightarrow&
        &\max_{I_s} f(x) < \max_{I_t} g(y) \\
        &\exists x'\, (x', \argmin_{I_t} g(y)) \in \gamma &\Rightarrow&
        &\exists x'\, &f(x') < \min_{I_t} g(y) &\Rightarrow&
        &\min_{I_s} f(x) < \min_{I_t} g(y) \\
      \end{aligned}
    \]

    In both cases we arrive at a contradiction.
  \end{enumerate}
\end{proof}

\begin{prop}[Generalised mountain climbing problem]\label{prop:climbing}
  Let $f, g \from I \to \R$ be piecewise linear maps.

  Given two points $(x, y)$ and $(x', y')$ in $I^2$ with $f(x) = g(y)$ and $f(x') = g(y')$, we are
  going to state conditions for the existence of a (continuous) function $\psi \from I \to I^2$ such that
  $\psi(0) = (x, y)$, $\psi(1) = (x', y')$, and $f \psi_1 = g \psi_2$.

  \begin{enumerate}
  \item for $(x, y) = (0, 0)$ and $(x', y') = (1, 1)$, $\psi$ exists if and only if the
    conditions (1), (2), (3a), and (3c) from~\cref{prop:curves} are satisfied;
  \item for $(x, y) = (0, 0)$ and $(x', y') = (0, 1)$, $\psi$ exists if and only if the
    conditions (2), (3a), and (3d) from~\cref{prop:curves} are satisfied;
  \item for $(x, y) = (0, 0)$ and $(x', y') = (1, 0)$, $\psi$ exists if and only if the
    conditions (1), (3a), and (3b) from~\cref{prop:curves} are satisfied;
  \end{enumerate}
\end{prop}

\begin{proof}
  As before, let $F \from I^2 \to \R$ be a function defined as $F(x, y) = f(x) - g(y)$ and
  $P = F^{-1}(0)$.  Thus, the existence of $\psi$ is equivalent to the points $(x, y)$ and
  $(x', y')$ lying in the same connected component of $P$. By~\cref{lemma:sep}, it is equivalent to
  the non-existence of a curve in $I^2 \setminus P$ that separates $(x, y)$ and $(x', y')$.

  It is easy to see that the points $(0, 0)$ and $(1, 1)$ can only be separated by four types of
  curves: those connecting $AB$ with $CD$, $BC$ with $DA$, $AB$ with $DA$, and $BC$ with
  $CD$. Further,~\cref{prop:curves} provides the criteria for the non-existence of such curves.

  The proofs of the remaining statements are similar.
\end{proof}

\begin{remark}
  It is clear that, given triangulations of $I$ in which $f$ and $g$ are simplicial, it is
  sufficient to verify the conditions in~\cref{prop:curves} only in vertices of the induced grid in
  $I^2$. This, combined with the monotonicity of the minimum and maximum functions, might enable
  checking the existence of paths from~\cref{prop:climbing} in linear time.
\end{remark}

\begin{prop}\label{prop:climbing2}
  Let $f, g \from I \to \R$ be piecewise linear maps such that
  \begin{enumerate}[nosep]
  \item $f, g \geq 0$,
  \item $f(0) = g(0) = g(1) = 0$,
  \item $\max_{I} f(x) \geq \max_{I} g(y)$.
  \end{enumerate}

  Then there exists a function $\psi \from I \to I^2$ such that $\psi(0) = (0, 0)$, $\psi(1) = (0, 1)$,
  and $f \psi_{1} = g \psi_{2}$.
\end{prop}

\begin{proof}
  According to~\cref{prop:climbing}, it is enough to check the conditions (2), (3a), and (3d)
  from~\cref{prop:curves}. Since $\min_{I} f(x) = \min_{I} g(y) = 0$, the condition (2) is
  satisfied. Similarly, because $\min_{[0, s]} f(x) = \min_{[0, t]} g(y) = \min_{[t, 1]} g(y) = 0$
  for any $s$ and $t$, the conditions (3a) and (3d) are satisfied.
\end{proof}

\begin{lemma}\label{l:p_subtree}
  Let $f \from |T| \to I$ be a non-degenerate piecewise linear map from a tree to a segment. Let
  $x, y, z \in f^{-1}(0)$ be distinct points. Let $e \in E(T)$ be an edge such that $T \setminus \{
  e \} = T_{1} \sqcup T_{2}$, $x, y \in T_{1}$, and $z \in T_{2}$.

  Then if $\max_{T_{2}} f(t) \geq \max_{x \to y} f(s)$, the pairs $(x, z)$ and $(y, z)$ are
  connected in $T_{f}^{(2)}$.
\end{lemma}

\begin{proof}
  Let $\hat{z} \in V(T_{2})$ be a vertex where the maximal value $\max_{T_{2}} f(t)$ is
  reached. Consider two paths $x \to y$ in $T_{1}$ and $z \to \hat{z}$ in $T_{2}$ represented by
  functions $\phi \from I \to |T_{1}|$ and $\gamma \from I \to |T_{2}|$. Note that
  $f \phi, f \gamma \geq 0$, $f\phi(0) = f\phi(1) = f\gamma(0) = 0$, and
  $\max_{t} f\gamma(t) \geq \max_{t} f\phi(t)$. Therefore, we can apply~\cref{prop:climbing2} for
  $f\gamma$ and $f\phi$, and obtain a path $\psi \from I \to I \times I$ connecting $(0, 0)$ with
  $(0, 1)$ such that $f \gamma \psi_{1} = f \phi \psi_{2}$. Since these paths cannot intersect (they
  lie in different trees), this gives us a double path in $(T_{1} \sqcup T_{2})_{f}^{(2)} \subseteq
  T_{f}^{(2)}$ connecting $(x, z)$ with $(y, z)$.
\end{proof}

\begin{prop}
  Let $T$ be a tree and let $f \from |T| \to I$ be a liftable non-degenerate piecewise linear
  map. Assume $x, y, x', y' \in f^{-1}(0)$ are distinct points, and pairs $(x, y)$ and $(x', y')$
  lie in the same connected component of $T_{f}^{(2)}$.

  Then $(x, y)$ is connected to either $(x, y')$ or $(y', y)$ in $T_{f}^{(2)}$.
\end{prop}

\begin{proof}
  Let $M$ be the maximal value of $f$ on the double path $(x, y) \to (x', y')$ in $T_{f}^{(2)}$. Let
  $(\hat{x}, \hat{y})$ be the last point of the path $(x, y) \to (x', y')$ with
  $f(\hat{x}) = f(\hat{y}) = M$. Let $\phi$ and $\gamma$ be functions $I \to |T|$ representing
  shortest paths $\hat{x} \to x$ and $\hat{y} \to y'$, respectively. Note that the components of the
  double path $(x, y) \to (x', y')$ must visit every vertex of these shortest paths.

  We know that $\max_{I} f\phi = \max_{I} f\gamma = M$ and $\min_{I} f\phi = \min_{I} f\gamma =
  0$. Moreover, $\max_{[0, t]} f\phi = \max_{[0, s]} f\gamma = M$ and
  $\min_{[t, 1]} f\phi = \max_{[s, 1]} f\gamma = 0$ for every $t$ and $s$. Thus, the conditions (1),
  (2), (3a), and (3c) from~\cref{prop:curves} are satisfied, and by applying (1)
  from~\cref{prop:climbing}, we get a path $\psi \from I \to I^{2}$ going from $(0, 0)$ to $(1, 1)$
  with $f \phi \psi_{1} = f \gamma \psi_{2}$.

  If $\phi \psi_{1} \neq \gamma \psi_{2}$, this provides a double path connecting $(x, y)$ with
  $(x, y')$. Otherwise, since $\phi$ and $\gamma$ represent shortest paths $\hat{x} \to x$ and $\hat{y} \to
  y'$, these paths must intersect. Let $p$ be the intersection of these paths closest to
  $y'$, or, equivalently, $p = m(y', \hat{x}, x)$.\footnote{Note that it is possible that $p = \hat{y}$.}

  Now we have a path $x \to p \to \hat{y} \to y$ whose first component is a (shortest) subpath of
  $x \to \hat{x}$, the second component is a subpath of $y' \to y$, and the last component is the
  path $\hat{y} \to y$. Additionally, we have a path $p \to y'$, which is a subpath of
  $\hat{y} \to y'$.

  Let $p \leadsto y$ be the concatenation of $p \to \hat{y}$ and $\hat{y} \to y$, and denote by $e$
  the edge next to $p$ along $p \leadsto y$. Clearly, $T \setminus e = T_{1} \sqcup T_{2}$, where
  $T_{1}$ contains the vertices $x$, $p$, and $y'$, while $T_{2}$ contains $y$.

  \begin{lemma}\label{l:max_reached_after_p}
    $\max_{T_{2}} f = M$.
  \end{lemma}

  \begin{proof}
    When $p \neq \hat{y}$, we have $\hat{y} \in T_{2}$ and $f(\hat{y}) = M$. Otherwise, when
    $p = \hat{y}$, let us consider the double path $(x, y) \to (\hat{x}, \hat{y})$ as the
    composition $(x, y) \to (p, y_{p}) \to (\hat{x}, \hat{y} = p)$, where $(p, y_{p})$ is the first
    vertex of the form $(p, \cdot)$. There are two cases:
    \begin{enumerate}
    \item We may use the edge $e$ before reaching $(p, y_{p})$, so there is a vertex $(x_{p}, p)$
      before $(p, y_{p})$.
    \end{enumerate}

    Clearly, $\hat{x} \neq p$ and $y_{p} \neq p$, when $y_{p}
    \in (p \to \hat{y} \to y) \setminus p$ and $f(y_{p}) = f(p) = f(\hat{y}) = M$.
  \end{proof}

  Let us now fix a lifting $\wt{f} \from |T| \to I$ of $T$. We may assume without loss of generality
  that $\wt{f}_{2}(y) > \wt{f}_{2}(x)$. Consider four cases:

  \begin{description}
  \item[Case 1] $p \to y'$ does not intersect $(p \to \hat{y} \to y) \setminus p$, and $\wt{f}_{2}(y) >
    \wt{f}_{2}(y') > \wt{f}_{2}(x)$.

    It follows from the choice of $p$ that $p \to y'$ do not intersect
    $(x \to p) \setminus p$.\footnote{We have chosen $x \to p$ to be the smallest subpath of
      $x \to \hat{x}$ going from $x$ to $p$.} Thus, $p \to y'$ intersects
    $x \to p \to \hat{y} \to y$ only at $p$. From~\cref{l:max_reached_after_p} it follows that the
    maximal values is reached on $(p \to \hat{y} \to y) \setminus p$. Therefore,
    by~\cref{prop:climbing2}, the vertices $(x, y)$ and $(y', y)$ are connected.
  \item[Case 2] $p \to y'$ does not intersect $(p \to \hat{y} \to y) \setminus p$, and $\wt{f}_{2}(y') >
    \wt{f}_{2}(y) > \wt{f}_{2}(x)$.

    Let us show that this cannot occur. By~\cref{l:max_reached_after_p}, there exists a vertex
    $y_{p} \in (p \to \hat{y} \to y) \setminus p$ with $f(y_{p}) = M$. The shortest path between
    $y_{p}$ and $y$ in the subgraph $(p \to \hat{y} \to y) \setminus p$ divides the strip $[0, M]
    \times \R$ into ``lower'' and ``upper'' parts, where the ``lower'' part contains $x$ while the
    ``upper'' part contains $y'$.
  \end{description}
\end{proof}

\begin{thebibliography}{9}
\bibitem{mountain} Goodman, Jacob E.; Pach, J\'{a}nos; Yap, Chee-K. (1989),
  \href{https://maa.org/sites/default/files/pdf/upload_library/22/Ford/Goodman-Pach-Yap494-510.pdf}{Mountain
    climbing, ladder moving, and the ring-width of a polygon}.
\bibitem{moise} Edwin E. Moise (1977) \emph{Geometric Topology in Dimensions 2 and 3}.
\end{thebibliography}
\end{document}
