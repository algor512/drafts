\documentclass[a4paper,12pt,reqno,oneside]{amsart}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[dvipsnames]{xcolor}
\usepackage{typearea}
\usepackage{hyperref}
\usepackage[capitalise,nameinlink]{cleveref}
\usepackage{mathtools}
\usepackage{indentfirst}
\usepackage{enumitem}

\usepackage[colorinlistoftodos,prependcaption,textsize=tiny]{todonotes}

\crefformat{enumi}{#2(#1)#3}

\definecolor{color0}{RGB}{34,113,178}
\definecolor{color1}{RGB}{61,183,233}
\definecolor{color2}{RGB}{247,72,165}
\definecolor{color3}{RGB}{53,155,115}
\definecolor{color4}{RGB}{213,94,0}
\definecolor{color5}{RGB}{230,159,0}
\definecolor{color6}{RGB}{114,228,97}
\definecolor{color7}{RGB}{53,37,94}
\definecolor{color8}{RGB}{128,0,103}

\colorlet{blue}{color0}
\colorlet{pink}{color2}

\hypersetup{
  pagebackref,
  colorlinks,
  linkcolor=blue,
  citecolor=pink,
  urlcolor=pink
}

\theoremstyle{plain}
\newtheorem{prop}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\theoremstyle{definition}
\newtheorem*{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem*{remark}{Remark}

\setlength{\parindent}{0cm}
\setlength{\parskip}{0.5em}

\setlist{listparindent=\parindent,parsep=\parskip}

\newcommand{\crefp}[2]{\hyperref[#2]{\namecref{#1}~\labelcref*{#1}~(\ref*{#2})}}

\newcommand{\from}{\colon}
\newcommand{\xto}[1]{\xrightarrow{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}

\newcommand{\id}{\mathrm{id}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\HH}{\mathcal{H}}
\newcommand{\JJ}{\mathcal{J}}
\newcommand{\Conf}{\mathrm{Conf}}
\newcommand{\Mod}[1]{\ (\mathrm{mod}\ #1)}
% \DeclareEmphSequence{\bfseries,\itshape,\upshape}

\DeclareMathOperator{\pr}{pr}
\DeclareMathOperator{\sk}{sk}
\DeclareMathOperator{\st}{st}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\Int}{Int}
\DeclareMathOperator{\codim}{codim}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand{\maxl}[1]{\max\limits_{#1}}
\newcommand{\minl}[1]{\min\limits_{#1}}
\newcommand{\argmaxl}[1]{\argmax\limits_{#1}}
\newcommand{\argminl}[1]{\argmin\limits_{#1}}

\author{Alexey Gorelov}
\title{}

\begin{document}

\section*{My reflections on the not orientable case}

While writing this text, I discovered that Minc proved his theorem for the orientable (and even
oriented) case. As a result, I removed some parts of the text, which simplified it significantly;
however, before that, I was trying to figure out how to formalise all this orientation stuff in
combinatorial terms, in a way that would be convenient in definitions and proofs.

Of course, there is a standart approach: we fix a local orientation at each vertex (of a graph drawn
on a surface), and label each edge by $+1$ or $-1$ according to whether it connects vertices with
compatible local orientations or not. However, this approach literally doubles the lengths of proofs
and definitions: e.g., in the definition of a $k$-crossing we need to consider two cases, when the
involved double path goes through an even or odd number of negative edges. The same happens with all
other definitions and proofs. Moreover, it doesn't seem totally reasonable to me to fix the local
orientations, as it makes the picture we get not really invariant: what if we had chosen different
orientations?

Personally, I think it makes more sense to use a different approach based on so-called orientation
sheaves
(\href{https://en.wikipedia.org/wiki/Orientation\_sheaf}{https://en.wikipedia.org/wiki/Orientation\_sheaf}). For
our case, it is a purely combinatorial object: we just have a two-element set of orientations for
each vertex, and each path induces an isomorphism between the orientations of its
endpoints. Actually, as we are interested more in orientation-invariant properties (like existence
of interleaving quadruples of the endpoints of curves), it allows us not to fix orientations at all,
and I'm pretty sure it would reduce the lengths of proofs and definitions.

\section*{Approximations, combinatorial approximations, and $k$-crossings.}

Let $f \from P \to H$ be a locally injective map from a path graph $P$ to a graph $H$. Also let $H$
be embedded into an oriented surface $S$ by an embedding $i$. For each vertex of $H$, we have a
cyclic order on the half-edges starting at it.

\begin{definition}
  A \emph{combinatorial improximation} of the map $f \from P \to H$ with fixed cyclic orders at the
  vertices of $H$ is a collection of linear orders $<_{(v, w)}$, one per each half edge $(v, w)$ of
  $H$, on the sets $\{ \{x, y\} \in E(P)\ |\ f(\{x, y\}) = \{v, w\}\}$, such that the orders
  $<_{(v, w)}$ and $<_{(w, v)}$ are inverse to each other.
\end{definition}

Having a combinatorial improximation, for every vertex $v \in V(H)$, we can define a cyclic order
$[\cdot, \cdot, \cdot]_{v}$ on the set of half-edges of $P$ whose first endpoint is mapped to $v$ by
$f$ by glueing linear orders of the combinatorial approximation according to the cyclic order on the
half-edges starting at $v$.

\begin{definition}
  A combinatorial improximation is called \emph{a combinatorial approximation} if for every internal
  vertices $x, y \in V(P)$ mapped to the same vertex $v \in V(H)$ by $f$, the pairs of the
  half-edges starting at $x$ and at $y$ doesn't interleave with respect to the cyclic order
  $[\cdot, \cdot, \cdot]_{v}$.

  Namely, let $e_{1}, e_{2}$ be the half-edges starting at $x$, and let $g_{1}, g_{2}$ be the
  half-edges starting at $y$. We require that there are no assignments of $i, j, k, l \in \{1, 2\}$
  such that $[g_{i}, e_{k}, g_{j}]_{v}$ and $[e_{k}, g_{j}, e_{l}]_{v}$.
\end{definition}

\begin{theorem}
  A map $f \from P \to H$ is approximable by embeddings if and only if there exists a combinatorial approximation
  of it.
\end{theorem}

\begin{proof}[Sketchy proof]
  Suppose $f$ is approximable by embeddings. Take its R-approximation $\wt{f} \from P \to \HH$, and
  orient each 0-handle of $\HH$ according to the orientation of $S$. Note that there is a bijection
  between the half-edges of $H$ and the attaching segments $D_{v} \cap D_{\{v, w\}}$ of the
  1-handles $D_{\{v, w\}}$ of the ribbon graph $\HH$.  Let us denote by $I_{(v, w)}$ the segment
  $D_{v} \cap D_{\{v, w\}}$ corresponding to a half-edge $(v, w)$.

  Every image of a half-edge $(x, y)$ of $P$ where $f(x) = v$ intersects the boundary
  $\partial D_{v}$ exactly once at a point inside $I_{(v, w)}$. Thus, we can restrict the cyclic
  order on the points of $\partial D_{v}$ to the points $\wt{f}(P) \cap I_{(v, w)}$. This gives us a
  linear order $<_{(v, w)}$. After doing that for every half-edge $(v, w)$ of $H$, we get a
  combinatorial improximation of $f$. Indeed, the condition on the orders $<_{(v, w)}$ and
  $<_{(w, v)}$ easily follows from the construction. Moreover, there are no interleaves in the sense
  of the definition of the combinatorial approximation, as otherwise we would have an intersection
  inside $D_{v}$ in the R-approximation.

  Conversely, suppose we have a combinatorial approximation. Let us first take a ribbon graph $\HH$
  and orient each 0-handle according to the orientation of $S$. Note that if for each
  $v \in V(D_{v})$ we could find a local R-approximation,\footnote{That is, an R-approximation of
    $f \big|_{\st_{P} f^{-1}(v)}$.}, such that the restrictions of the cyclic order on
  $\partial D_{v}$ to $I_{(v, w)}$ produces $<_{(v, w)}$, we are done. Indeed, the conditions from
  the definition of the combinatorial improximation ensure that we are able to glue the local
  approximations we get, by modifying them inside the discs $D_{\{v, w\}}$, into an R-approximation.

  However, the non-interleaving condition allows us to do this. Let us consider $v \in V(H)$. First,
  we orient every length two path $a \to x \to b$ in $P$ with $f(x) = v$. Next, we correspond to
  each such path an arc $A_{x} = \{p\ |\ [a, p, b]_{v}\} \subset \partial D_{v}$ consisting of the points
  $p$ lying between $a$ and $b$ with respect to the cyclic order on $\partial D_{v}$. The arcs form
  a poset (by inclusion), and it is easy to inductively embed the corresponding double paths into
  $\st_{\HH} D_{v}$ starting from the minimal elements. And finally, we can embed the stars of degree one
  vertices mapped to $v$ by $f$.
\end{proof}

\begin{definition}
  A \emph{double path} between pairs of vertices $(x_{0}, y_{0}), (x_{l}, y_{l}) \in V(P)^{2}$ is a pair of paths
  \[
    \begin{aligned}
      &x_{0} \to x_{1} \to \dots \to x_{l-1} \to x_{l} \\
      &y_{0} \to y_{1} \to \dots \to y_{l-1} \to y_{l}
    \end{aligned}
  \]
  of equal length $l$ such that $x_{i} \neq y_{i}$, $x_{i} \neq x_{i+1}$, $y_{i} \neq y_{i+1}$, and
  $f(x_{i}) = f(y_{i})$ for every $i \in \overline{0,l}$.
\end{definition}\todo{Maybe it would be convenient to include $x_{-1}$, $y_{-1}$, $x_{l+1}$, $y_{l+1}$
  into the double path.}

\begin{definition}
  Assume we have a double path of length $k$ between two pairs $(x_{0}, y_{0}), (x_{l}, y_{l})$ of vertices of $P$
  of degree two. Let
  \begin{enumerate}[nosep]
  \item $x_{0}$ lies on a 2-path $x_{-1} \to x_{0} \to x_{1}$,
  \item $y_{0}$ lies on a 2-path $y_{-1} \to y_{0} \to y_{1}$,
  \item $x_{l}$ lies on a 2-path $x_{l-1} \to x_{l} \to x_{l+1}$,
  \item $y_{l}$ lies on a 2-path $y_{l-1} \to y_{l} \to y_{l+1}$.
  \end{enumerate}

  Additionally, let
  \begin{enumerate}[nosep]
  \item $f(x_{0}) = f(y_{0}) = v$ and $f(x_{l}) = f(y_{l}) = w$,
  \item $f(x_{1}) = f(y_{1}) = s$,
  \item $f(x_{l-1}) = f(y_{l-1}) = t$,
  \item $f(x_{-1}) = a$ and $f(y_{-1}) = b$,
  \item $f(x_{l+1}) = c$ and $f(y_{l+1}) = d$.
  \end{enumerate}

  We say that the double path realises a \emph{(combinatorial) $k$-crossing} if
  $[(v, b), (v, s), (v, a)]_{v}$ and $[(w, d), (w, t), (w, c)]_{v}$.
\end{definition}

\begin{lemma}\label{l:nocrossings}
  If $f$ is approximable, there are no $k$-crossings for any $k \geq 0$.
\end{lemma}

\begin{proof}
  Assume we have a combinatorial $k$-crossing realised by a double path $D = P_{1} \sqcup P_{2} \to
  H$. Let $J = f(P_{1}) = f(P_{2})$ be a path in $H$. It is clear that once we have an R-approximation
  $\wt{f} \from P \to \HH$, we have an R-approximation $D \to \JJ \to \HH$, where $\JJ$ is the
  ribbon graph of the path $J$ (that is, a disc), and the second map restricts to homeomorphisms
  that send the handles of $\JJ$ to handles of $\HH$.

  It can be seen that, if $D$ realises a $k$-crossing, pairs of arcs $(I_{(v, a}), I_{(v, c)})$,
  $(I_{(v, b}), I_{(v, d)})$ interleave with respect to a cyclic order induced by an orientation
  on $\partial \JJ$. Therefore, the curves $\wt{f}(\st_{P} P_{1})$ and $\wt{f}(\st_{P} P_{2})$ must
  intersect, and we have arrived at a contradiction.
\end{proof}

\begin{theorem}\label{thm:crossings}
  $f$ is approximable if and only if there are no $k$-crossings for any $k \geq 0$.
\end{theorem}

The following theorem is proven in \cite{minc}:
\begin{theorem}[{\cite[Theorem 4.11]{minc}}]\label{thm:minc}
  The following statements are equivalent:
  \begin{enumerate}
  \item $f \from P \to H$ is approximable by embeddings.
  \item $L^{n}(f)$ do not have a 0-crossing for each $n \geq 0$. Here $L^{n}(f)$ is an $n$-th
    derivative of $f$ defined in~\cite[Definition 3.3]{minc}.
  \end{enumerate}
\end{theorem}

Thus, in order to prove the theorem we have stated, we need to show that once we have a 0-crossing
in $L^{n}(f)$, we have to have a $k$-crossing in $f$ for some $k$.

First, we need to recall the definition of $L(f)$. Actually, in our case (of locally injective
maps), this is just a map between the line graphs of $P$ and $H$: $L(f) \from L(P) \to
L(H)$\footnote{This is because connected components $C$ of $f^{-1}(e)$, whose images are edges, are edges.} that takes a vertex $e$
of $L(P)$ to the vertex $f(e)$ of $L(H)$.

However, based on the cyclic orders of half-edges at the vertices of $H$, we need to define cyclic
orders of half-edges at the vertices of $L(H)$. Assume $e$ is a vertex of $L(H)$ that represents an
edge $e = \{v, w\}$ of $H$. Let
\begin{enumerate}[nosep]
\item $(v, w), (v, a_{1}), (v, a_{2}), \dots, (v, a_{n}), (v, w)$ be the cyclic order of the
  half-edges at $v$,
\item $(w, v), (w, b_{1}), (w, b_{2}), \dots, (w, b_{m}), (w, v)$ be the cyclic order of the
  half-edges at $w$.
\end{enumerate}

The half-edges starting at $e$ in $L(H)$ go to the vertices representing $e_{i} = \{v, a_{i}\}$ and
$g_{i} = \{v, b_{j}\}$. In order to obtain the needed cyclic order at $e$, we just remove $(v, w)$
from the cyclic order at $v$, remove $(w, v)$ from the cyclic order at $w$, glue these two linear
orders into a cyclic order $(v, a_{1}), (v, a_{2}), \dots, (v, a_{n}), (w, b_{1}), (w, b_{2}),
\dots, (w, b_{m}), (v, a_{i})$, and then replace every half-edge $(x, y)$ with the half-edge $(e,
{x, y})$, where ${x, y}$ is a vertex of $V(H)$. Finally, we obtain the cyclic order $(e, e_{1}), (e,
e_{2}), \dots, (e, e_{n}), (e, g_{1}), \dots, (e, g_{m}), (e, e_{1})$.

More formally, let $X$ and $Y$ be two cyclically ordered sets, and fix $x \in X$ and $y \in Y$. Let
$X \vee_{x \sim y} Y$ be the set $(X \sqcup Y) \setminus \{x, y\}$. Let $\pr_{X} \from X \vee_{x
  \sim y} Y \to X$ be a map such that $\pr_{X}(p) = p$ if $p \in X$ and $\pr_{X}(p) = x$
otherwise. In a similar way let us define a map $\pr_{Y} \from X \vee_{x \sim y} Y \to Y$.

Take a cyclic order $[\cdot, \cdot, \cdot]_{X \vee_{x \sim y} Y}$ on $X \vee_{x \sim y} Y$ such that
  $[a, b, c]_{X \vee_{x \sim y} Y}$ if and only if either
\begin{enumerate}[nosep]
\item $a, b, c \in X$ and $[a, b, c]_{X}$,
\item $a, b, c \in Y$ and $[a, b, c]_{Y}$,
\item $|\{a, b, c\} \cap X| = 2$ and $[\pr_{X}(a), \pr_{X}(b), \pr_{X}(c)]_{X}$,
\item $|\{a, b, c\} \cap Y| = 2$ and $[\pr_{Y}(a), \pr_{Y}(b), \pr_{Y}(c)]_{Y}$.
\end{enumerate}

Finally, put $E_{\{a, b\}}(L(H)) = \phi\left(E_{a}(H) \vee_{(a, b) \sim (b, a)} E_{b}(H)\right)$ (as
two cyclically ordered sets) where $E_{p}(G)$ is the set of half-edges starting at $p$ in $G$, and
$\phi$ is a bijection
$E_{a}(H) \sqcup E_{b}(H) \setminus \{(a, b), (b, a)\} \cong E_{\{a, b\}}(L(H))$ that takes
half-edges $(a, x)$ to $(\{a, b\}, \{a, x\})$ and half-edges $(b, x)$ to $(\{a, b\}, \{b, x\})$.

\begin{lemma}\label{l:locinj}
  If $f \from P \to H$ is a locally injective map, then $L(f) \from L(P) \to L(H)$ is locally
  injective.
\end{lemma}

\begin{proof}
  Suppose it is not, so there are two edges $\{e, g_{1}\}, \{e, g_{2}\} \in E(P)$ sharing a vertex $e
  \in V(P)$ with the same image under $L(f)$. Let $e = \{a, b\}$ as an edge of $P$. Therefore, $g_{1}
  = \{a, t\}$ and $g_{2} = \{b, s\}$ as edges of $P$. Since $L(f)(g_{1}) = L(f)(g_{2})$, we have $f(g_{1}) =
  f(g_{2})$. Therefore, $f(t) = f(b)$ and $f(s) = f(a)$ because $f(a) \neq f(b)$, and we have a
  2-path $b \to a \to t$ where $f(b) = f(t)$, which implies that $f$ is not locally injective, and
  we have a contradiction.
\end{proof}

\begin{lemma}
  Let $(e_{0} \to e_{l}, g_{0} \to g_{l})$ be a double path of length $l \geq 0$ in $L(P)$, where
  $e_{i} = \{a_{i}, a_{i+1}\}$ and $e_{i} = \{b_{i}, b_{i+1}\}$ (as edges of $P$). Then $(a_{0} \to
  a_{l+1}, b_{0} \to b_{l+1})$ is a double path of length $l+1$ in $P$.
\end{lemma}

\begin{proof}
  First note that $e_{i}$ and $e_{i+1}$ ($g_{i}$ and $g_{i+1}$) as edges of $P$ must share a vertex,
  so the notation is well-defined: let $a_{0}$ and $b_{0}$ be the endpoints of $e_{0}$ and $g_{0}$
  that are not endpoints of $e_{i}$ and $g_{i}$, respectively, let $a_{i}$ and $b_{i}$ be $e_{i-1}
  \cap e_{i}$ and $g_{i-1} \cap g_{i}$, and let $a_{l+1}$ and $b_{l+1}$ be $e_{l} \setminus e_{l-1}$ and
  $g_{l} \setminus g_{l-1}$.

  Clearly, $f(a_{0}) = f(e_{0}) \setminus f(e_{1}) = f(b_{0})$,
  $f(a_{l+1}) = f(e_{l}) \setminus f(e_{l-1}) = f(b_{l+1})$, and
  $f(a_{i}) = f(e_{i-1}) \cap f(e_{i}) = f(b_{i})$. Finally, if $a_{i} = b_{i}$, we have either a
  2-path $a_{i-1} \to a_{i} = b_{i} \to b_{i-1}$ or a 2-path $a_{i+1} \to a_{i} = b_{i} \to b_{i+1}$
  whose endpoints have the same image, which contradicts our assumption that $f$ is locally injective.
\end{proof}

\begin{lemma}\label{l:derivative_crossings}
  Assume $L(f)$ has a $k$-crossing for $k \geq 0$. Then $f$ has a $k+1$-crossing.
\end{lemma}

\begin{proof}
  Let us construct, given a $k$-crossing of $L(f)$, a $k+1$-crossing of $f$. Let $(e_{0} \to e_{k},
  g_{0} \to g_{k})$ be a double path realising a $k$-crossing for $L(f)$. As in the definition of a
  $k$-crossing, let $e_{-1}$ and $g_{-1}$ be neighbours of $e_{0}$ and $g_{0}$, other than $e_{1}$
  and $g_{1}$. Similarly, let $e_{k+1}$ and $g_{k+1}$ be neighbours of $e_{k}$ and $g_{k}$, other than $e_{k-1}$
  and $g_{k-1}$.

  Let $l_{i} = f(e_{i}) = f(g_{i})$ for $i = \overline{0, k}$. Since the double path realises a
  $k$-crossing, we know that
  \begin{enumerate}[nosep]
    \item $l_{-1}^{e} = L(f)(e_{-1}) \neq L(f)(g_{-1}) = l_{-1}^{g}$,
    \item $l_{k+1}^{e} = L(f)(e_{k+1}) \neq L(f)(g_{k+1}) = l_{k+1}^{g}$,
    \item $[(l_{0}, l_{-1}^{g}), (l_{0}, l_{1}), (l_{0}, l_{-1}^{e})]_{l_{0}}$,
    \item $[(l_{k}, l_{k+1}^{g}), (l_{k}, l_{k-1}), (l_{k}, l_{k+1}^{e})]_{l_{k}}$.
  \end{enumerate}

  From the previous lemma it follows that the double path $(e_{0} \to e_{k}, g_{0} \to g_{k})$
  induces a double path $(a_{0} \to a_{k+1}, b_{0} \to b_{k+1})$ of length $k+1$ in $P$. Moreover,
  there exist vertices $a_{-1}$, $b_{-1}$, $a_{k+2}$, and $b_{k+2}$, such that
  $e_{-1} = \{a_{-1}, a_{0}\}$, $g_{-1} = \{b_{-1}, b_{0}\}$, $e_{k+1} = \{a_{k+1}, a_{k+2}\}$, and
  $g_{k+1} = \{b_{k+1}, b_{k+2}\}$. Let $v_{i} = f(a_{i}) = f(b_{i})$ for $i \in \overline{0,
    k+1}$. Note that it suffices to prove that
  $[(v_{0}, f(b_{-1})), (v_{0}, v_{1}), (v_{0}, f(a_{-1}))]_{v_{0}}$ and
  $[(v_{k+1}, f(b_{k+2})), (v_{k+1}, v_{k}), (v_{k+1}, f(a_{k+2}))]_{v_{k+1}}$.  It will follow from
  the next lemma, the proof of which completes the proof of this one.
\end{proof}

\begin{lemma}
  Assume we have two 2-paths $e_{0} \to e_{1} \to e_{2}$ and $g_{0} \to g_{1} \to g_{2}$ in $L(f)$,
  with $l_{0} = L(f)(e_{0}) = L(f)(g_{0})$, $l_{1} = L(f)(e_{1}) = L(f)(g_{1})$, and $l_{e} = L(f)(e_{2}) \neq
  L(f)(g_{2}) = l_{g}$. Let $e_{i} = (a_{i}, a_{i+1})$ and $g_{i} = (b_{i}, b_{i+1})$, so we have two 3-paths
  $a_{0} \xto{e_{0}} a_{1} \xto{e_{1}} a_{2} \xto{e_{2}} a_{3}$ and $b_{0} \xto{g_{0}} b_{1}
  \xto{g_{1}} b_{2} \xto{g_{2}} b_{3}$ with $f(a_{i}) = f(b_{i}) = v_{i}$ for $i = 0, 1, 2$ and
  $v_{a} = f(a_{3}) \neq f(b_{3}) = v_{b}$. Then the cyclic order on $\{(l_{1}, l_{0}), (l_{1},
  l_{e}), (l_{1}, l_{g})\}$ at $l_{1} \in V(L(H))$ coincides with the cyclic order on $\{(v_{2},
  v_{1}), (v_{2}, v_{a}), (v_{2}, v_{b})\}$ at $v_{2} \in V(H)$.
\end{lemma}

\begin{proof}
  Note that $l_{1}$ corresponds to an edge $\{v_{1}, v_{2}\}$ in $H$. Thus, the half-edges starting
  at $l_{1}$ come from two sources: half-edges starting at $v_{1}$ and half-edges starting at
  $v_{2}$. Formally, using the notation from the definition of the cyclic order on the half-edges at
  $l_1$, we have a bijection $\phi \from E_{v_{1}}(H) \sqcup E_{v_{2}}(H) \setminus \{ (v_{1}, v_{2}),
  (v_{2}, v_{1}) \} \to E_{l_{1}}(L(H))$. Additionally, we have ``projections'' $\pr_{v_{1}} \from
  E_{l_{1}}(L(H)) \to E_{v_{1}}(H)$ and $\pr_{v_{1}} \from E_{l_{1}}(L(H)) \to E_{v_{2}}(H)$, which
  are $\pr_{E_{v_{1}}(H)}$ and $\pr_{E_{v_{2}}(H)}$ from the definition of the cyclic order, respectively.

  In our case, the half-edges $(l_{1}, l_{e})$ and $(l_{1}, l_{g})$ correspond to the half-edges
  $(v_{2}, v_{a})$ and $(v_{2}, v_{b})$, while the half-edge $(l_{1}, l_{0})$ corresponds to the
  half-edge $(v_{1}, v_{0})$. Thus, we have $\pr_{v_{2}}((l_{1}, l_{e})) = (v_{2}, v_{a})$,
  $\pr_{v_{2}}((l_{1}, l_{g})) = (v_{2}, v_{b})$, and $\pr_{v_{2}}((l_{1}, l_{0})) = (v_{2},
  v_{1})$. Now the statement of the lemma follows from two last conditions in the definition of the
  cyclic order at $l_{1}$.
\end{proof}

\begin{proof}[Proof of~\cref{thm:crossings}]
  If $f$ is approximable, there are no $k$-crossings by~\cref{l:nocrossings}. Suppose $f$ is not
  approximable, thus, by~\cref{thm:minc}, $L^{n}(f)$ contains a 0-crossing, for some $n \geq
  0$. By~\cref{l:locinj} all maps $L^{k}(f)$ for $k = \overline{1,n}$ are locally injective, and
  finally, by applying~\cref{l:derivative_crossings} $n$ times we conclude that $f$ contains an
  $n$-crossing.
\end{proof}

\section*{Combinatorial approach to Minc theorem}

It may seem that it would be easier to prove~\cref{thm:crossings} without using Minc theorem. It
seems to me that it is not so simple, because (as a result of our discussions with Francis) I have
the impression that using the operation $L$ is the most natural way to prove the theorem. However,
it seems that, at least for locally injective maps, it is possible to prove Minc theorem in a
purely combinatorial way, and the present section describes (in a sketchy way) how to do this.

\begin{lemma}[Step forward]\label{l:forward}
  If $f$ is combinatorially approximable, then so is $L(f)$.
\end{lemma}

\begin{proof}[Sketch of proof]
 First, let us fix a combinatorial approximation of $f$; we are going to use it to construct a
 combinatorial approximation of $L(f)$.

 Notice that the half-edges of $L(P)$ that are mapped to a half-edge $(\{a, b\}, \{b, p\})$ of
 $L(H)$ are in bijection with the half-edges $(x, y)$ where $f(x) = b, f(y) = p$ such that $x$ is
 adjacent to an edge $(z, x)$ with $f(z) = a$.\footnote{In other words, we have a 2-path $z \to x
   \to y$ mapped to a 2-path $a \to b \to p$ by $f$} Therefore, to define $<_{(\{a, b\}, \{b,
   p\})}$, we can just use $<_{(b, p)}$. It can be checked that $<_{(\{p, b\}, \{b, a\})}$ are
 inverse to each other; indeed, $<_{(\{p, b\}, \{b, a\})}$ is based on $<_{(b, a)}$, and it is easy to
 see that if we have two 2-paths $x \to y \to z$ and $x' \to y' \to z'$ that are mapped to $a \to b
 \to b$, then the orders in pairs $(y, x), (y', x')$ and $(y, z), (y', z')$ are inverse to each
 other.\footnote{Here we must use the non-interleaving condition.}

 To see that this gives us a combinatorial approximation, we can note that once we have a pair of
 interleaving 2-paths in $L(P)$ centered at $\{a, b\}$, we have a pair of ``intersecting''
 (geometrically) 3-paths in $P$ whose central edges are mapped to $\{a, b\}$ by $f$, so we its
 parts centered at $a$ or at $b$ must interleave.
\end{proof}

\begin{lemma}[Step backward]\label{l:backward}
  If $L(f)$ is combinatorially approximable, and $f$ doesn't have a 0-crossing, then $f$ is
  combinatorially approximable.
\end{lemma}

\begin{proof}[Sketch of proof]
  First we need to define $<_{(a, b)}$ for every half-edge $(a, b)$ of $H$. Note that every
  half-edge $(x, y)$ with $f((x, y)) = (a, b)$, only except only the case when $x$ is a degree one
  vertex, lies on a 2-path $z \to x \to y$ where $f(z) = p \notin \{a, b\}$. In turn, every such 2-path
  represent a half-edge in $L(P)$ mapped to $(\{p, a\}, \{a, b\})$. Thus, we just glue linear orders
  $<_{(\{p, a\}, \{a, b\})}$ with respect to the linear order on $E_{a}(P) \setminus \{(a, b)\}$,
  and then we invert the resulting order to get a part of $<_{(a, b)}$ that is defined on all
  half-edges $(x, y)$ where $x$ is not an endpoint of $P$.\footnote{That is, after removing such
    half-edges we obtain a linear order.}

  Next we do the same for $(b, a)$, and we get a part of $<_{(b, a)}$. Note that, except the trivial
  case of length one $P$, we cannot have half-edges $(x, y)$ and $(y, x)$ on which both $<_{(b, a)}$
  and $<_{(a, b)}$ are not defined. Moreover, using the assumption that we use a combinatorial
  approximation of $L(f)$, it could be checked that the orders $<_{(a, b)}$ and $<_{(b, a)}$ are
  inverse to each other. Therefore, after completing them in accordance with each other, we get a
  combinatorial improximation.

  Finally, suppose that we have a pair of interleaving 2-paths $x \to y \to z$ and
  $x' \to y' \to z'$ whose central vertices are mapped to $a$ by $f$. By the assumtions, $f$ doesn't
  have a 0-crossing, so their images must share an edge, say $f((x, y)) = f((x', y')) = (b, a)$.

  In the case when $c = f(z) = f(z')$, so the images of the 2-paths coincide, they are represented by two
  edges $\{\{x, y\}, \{y, z\}\}, \{\{x', y'\}, \{y', z'\}\}$ in $L(P)$ that have the same image
  $\{\{b, a\}, \{a, c\}\}$ in $L(H)$. The linear orders $<_{(a, c)}$ and $<_{(a, b)}$ are based on
  the linear orders $<_{(\{b, a\}, \{a, c\})}$ and $<_{(\{c, a\}, \{a, b\})}$. Using the fact that
  these orders are inverse to each other, one can arrive to a contradiction.

  When $f(z) = c \neq c' = f(z')$, we have $L(f)((\{x, y\}, \{y, z\}\})) = (\{b, a\}, \{a, c\})$ and
  $L(f)((\{x', y'\}, \{y', z'\})) = (\{b, a\}, \{a, c'\})$. Observe that $<_{(a, b)}$ is obtained by
  gluing the orders $<_{(\{c, a\}, \{a, b\})}$ and $<_{(\{c', a\}, \{a, b\})}$ (among others)
  according to the linear order on $E_{a}(P) \setminus \{(a, b)\}$, see the construction
  above. Therefore, the order in the pair of half-edges $(y, z), (y', z')$ must be consistent with
  the order of $(a, c)$ and $(a, c')$ in $E_{a}(P) \setminus \{(a, b)\}$, and we cannot have an
  interleaving.

  Thus, if we have a pair of interleaving 2-paths, we must have a 0-crossing. This concludes the proof.
\end{proof}

\begin{proof}[Proof of Minc theorem for locally injective maps]
  Let $f \from P \to H$ be a locally injective map. If $f$ is approximable then by~\cref{l:forward}
  all $L^{n}(f)$'s are approximable.

  Suppose that $f$ is not approximable. Note that for sufficiently large $n$ we have $L^{n}(f) =
  0$. Let $k \geq 0$ be a maximal number such that $L^{k}(f)$ is not approximable. It follows that
  $L^{k+1}(f)$ is approximable, and, thus, by~\cref{l:backward}, $L^{k}(f)$ has a 0-crossing.
\end{proof}

\begin{thebibliography}{9}
  \bibitem{minc} Minc, Piotr. ``Embedding simplicial arcs into the plane.'' Topol. Proc. Vol. 22. 1997.
\end{thebibliography}
\end{document}