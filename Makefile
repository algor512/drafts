double_paths.pdf: double_paths.tex
	latexmk -pdf $^

contractions.pdf: contractions.tex
	latexmk -pdf $^

.PHONY: all
all: double_paths.pdf contractions.pdf

.PHONY: clean
clean:
	latexmk -C
	rm -Rf auto/
